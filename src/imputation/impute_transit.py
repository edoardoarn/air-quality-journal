import os
import argparse
import numpy as np 
import pandas as pd

from matplotlib import pyplot as plt
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer


DEFAULT_DATA_PATH = "D:\\Datasets\\air-quality\\processed\\transits\\data_merged.csv"
DEFAULT_OUTPUT_PATH = "D:\\Datasets\\air-quality\\processed\\transits\\data_imputed.csv"
SEED = 42


def find_missing_periods(df, feature):
    periods = []
    dates = df[df[feature].notna()].index
    diff = np.diff(dates) # a[i+1] - a[i]
    indices = np.argwhere(diff > np.timedelta64(1, 'h')) # these are the i-th index in dates, needs also i+1
    for i in indices:
        periods.append(tuple([dates[i], dates[i+1]]))
    return periods


def impute_mean(ts, start : pd.Timestamp, end : pd.Timestamp, window):
    """
    Weird function that, given a missing period, gets whatever years are available during the same period,
    averages them to the same value and scales it using data left and right of the hole with a size given by window.
    """
    yd = np.timedelta64(365, "D")
    # get 2013
    multiplier = (start.year - 2015 + 2)
    start1 = start - (yd * multiplier)
    end1 = end - (yd * multiplier)
    ts1 = ts[start1[0]:end1[0]]

    # get 2014 or 2015 based on current year
    start2 = start - yd if multiplier == 2 else start + yd
    end2 = end - yd if multiplier == 2 else end + yd
    ts2 = ts[start2[0]:end2[0]]
    
    # check if data is effectively present
    assert np.all(ts1.isna().sum() == 0) and np.all(ts2.isna().sum() == 0)
    #average vals, standardize and scale to surrounding mean and std
    avg = (ts1.values + ts2.values) / 2.0
    avg = (avg - avg.mean(axis=0)) / avg.std(axis=0)
    ls = ts[start[0] - window:start[0]]
    rs = ts[end[0]:end[0] + window]
    curr_mean =(ls.mean(axis=0) + rs.mean(axis=0)) / 2
    curr_std = (ls.std(axis=0) + rs.std(axis=0)) / 2

    scaled_avg = (avg * curr_std) + curr_mean
    index = pd.DatetimeIndex(pd.date_range(start[0], end[0], freq="H"))
    result = pd.Series(data=scaled_avg, index=index)
    return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Takes the given averaged dataset and imputes columns")
    parser.add_argument("--path", "-p", type=str, default=DEFAULT_DATA_PATH)
    parser.add_argument("--out", "-o", type=str, default=DEFAULT_OUTPUT_PATH)
    args = parser.parse_args()

    # load data that might contain holes in the time series
    raw_data = pd.read_csv(args.path, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    index = pd.date_range(start="2013-01-01",end="2016-01-01", freq="H")
    assert len(index) == 26281 # assert index length is equal to # of hours in 3 years + 1
    
    # create the complete dataframe
    data = pd.DataFrame(index=index, columns=raw_data.columns)
    for c in data.columns:
        data.loc[raw_data.index, c] = raw_data[c].values

    # count missing
    nulls = data.isna().sum()
    incomplete_features = []
    for feature, count in zip (data.columns, nulls):
        percent = count/len(data) * 100.0
        print(f"{feature:<10s}: {count:<6d} - {percent:.4f}%")
        if (count > 0):
            incomplete_features.append((feature, percent))
    
    # set a max tollerance for 30 days of nans
    max_delta = np.timedelta64(30, "D")
    window = np.timedelta64(5, "D")

    # find missing periods for each feature
    for col in data.columns:
        print(f"imputing {col}...")
        periods = find_missing_periods(data, col)
        for p in periods:
            delta = p[1] - p[0]
            if delta >= max_delta:
                continue
            imp = impute_mean(data[col], p[0], p[1], window)
            data.at[imp.index, col] = imp.values

    # check no nulls except the last ones
    print(data.isna().sum())

    # save to file
    data.to_csv(args.out, index=True)