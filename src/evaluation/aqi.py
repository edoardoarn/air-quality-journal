import os
import argparse
import pandas as pd
import numpy as np
import glob
from matplotlib import pyplot as plt

from sklearn.metrics import f1_score, accuracy_score, confusion_matrix

from src.config import TrainingConfig


config = TrainingConfig(argparse.Namespace(data="data_cleaned_cycle_h_rbf"))
pretty_names = {
    "model": "model",
    "mae_score": "MAE",
    "rmse_score": "RMSE",
    "r2_score": "R^2",
    "smape_score": "sMAPE"
}


def print_latex(pollutant, table):
    columns = table.columns.values
    print("\hline")
    print(f"\multicolumn{{{len(columns)}}}{{|c|}}{{{pollutant.upper()}}}")
    print(f"{' & '.join([pretty_names[c] for c in columns])} \\\\")
    print("\hline")
    for idx, row in table.iterrows():
        vals = " & ".join([f"{v:.2f}" if type(v) == float else v for v in row])
        print(f"{vals} \\\\")
    print("\hline")


def print_cm(cm):
    assert len(cm.shape) == 2
    print("\tpred")
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            print(f"{cm[i,j]:d}", end="")
            if j < cm.shape[1] - 1:
                print(" ", end="")
        print("")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate AQI")
    parser.add_argument("--format", type=str, choices=["none", "latex"], default="none")
    parser.add_argument("--window", type=str, choices=["0", "24", "48", "n"], default="0")
    parser.add_argument("--model", type=str, default="bridge")
    args = parser.parse_args()
    labels = list(range(5))

    subdir = f"_w{args.window}"
    no2_file = f"{args.model}_w{args.window}_no2.csv"
    o3_file  = f"{args.model}_w{args.window}_o3.csv"
    pm10_file = f"{args.model}_w{args.window}_pm10.csv"

    no2_df = pd.read_csv(os.path.join(config.PROJECT_PATH, "aqi", subdir, no2_file), 
                    index_col=0, 
                    parse_dates=True, 
                    date_parser=lambda x: pd.to_datetime(x))
    o3_df = pd.read_csv(os.path.join(config.PROJECT_PATH, "aqi", subdir, o3_file),
                    index_col=0, 
                    parse_dates=True, 
                    date_parser=lambda x: pd.to_datetime(x))
    daily_pm10_df = pd.read_csv(os.path.join(config.PROJECT_PATH, "aqi", subdir, pm10_file),
                    index_col=0, 
                    parse_dates=True, 
                    date_parser=lambda x: pd.to_datetime(x))
    
    # dpm10_true = pd.cut(daily_pm10_df.y_true, bins=config.AQI_LEVELS["pm10"], labels=labels)
    # dpm10_pred = pd.cut(daily_pm10_df.y_pred, bins=config.AQI_LEVELS["pm10"], labels=labels)
    pm10_df = daily_pm10_df.resample("H").pad()

    print(no2_df.shape, o3_df.shape, pm10_df.shape)

    assert no2_df.isna().sum().sum() == 0
    assert o3_df.isna().sum().sum() == 0
    assert pm10_df.isna().sum().sum() == 0

    start = max(no2_df.index.min(), o3_df.index.min(), pm10_df.index.min())
    end = min(no2_df.index.max(), o3_df.index.max(), pm10_df.index.max())
    print(start, end)

    no2_df = no2_df.loc[start:end]
    o3_df = o3_df.loc[start:end]
    pm10_df = pm10_df.loc[start:end]
    
    print(len(config.AQI_LEVELS["no2"]))
    no2_df.y_pred.clip(lower=0, upper=no2_df.y_pred.max(), inplace=True)
    o3_df.y_pred.clip(lower=0, upper=o3_df.y_pred.max(), inplace=True)
    pm10_df.y_pred.clip(lower=0, upper=pm10_df.y_pred.max(), inplace=True)
    
    assert all(no2_df.y_pred >= 0)
    assert all(o3_df.y_pred >= 0)

    index_no2_true = pd.cut(no2_df.y_true, bins=config.AQI_LEVELS["no2"], labels=labels)
    index_no2_pred = pd.cut(no2_df.y_pred, bins=config.AQI_LEVELS["no2"], labels=labels)
    index_o3_true = pd.cut(o3_df.y_true, bins=config.AQI_LEVELS["o3"], labels=labels)
    index_o3_pred = pd.cut(o3_df.y_pred, bins=config.AQI_LEVELS["o3"], labels=labels)    
    index_pm10_true = pd.cut(pm10_df.y_true, bins=config.AQI_LEVELS["pm10"], labels=labels)
    index_pm10_pred = pd.cut(pm10_df.y_pred, bins=config.AQI_LEVELS["pm10"], labels=labels)

    # for a, b in zip(index_no2_true.values, index_no2_pred.values):
    #     print(f"{a:3.1f} {b:3.1f}")
    # i = np.where(pd.isna(index_no2_pred))
    # print(index_no2_true.iloc[i], index_no2_pred.iloc[i])

    aqi_true= [max(a, b, c) for a, b, c in zip(index_no2_true, index_o3_true, index_pm10_true)]
    aqi_pred = [max(a, b, c) for a, b, c in zip(index_no2_pred, index_o3_pred, index_pm10_pred)]
    aqi2_true= [max(a, b) for a, b, c in zip(index_no2_true, index_o3_true, index_pm10_true)]
    aqi2_pred = [max(a, b) for a, b, c in zip(index_no2_pred, index_o3_pred, index_pm10_pred)]

    print(f"model: {args.model} - window: {args.window}")
    print(f"f-score (no2) : {f1_score(index_no2_true, index_no2_pred, average='micro'):.4f}")
    print(f"f-score (o3)  : {f1_score(index_o3_true, index_o3_pred, average='micro'):.4f}")
    print(f"f-score (pm10): {f1_score(index_pm10_true, index_pm10_pred, average='micro'):.4f}")
    print(f"f-score AQI   : {f1_score(aqi_true, aqi_pred, average='micro')}")
    cm1 = confusion_matrix(aqi_true, aqi_pred, labels=labels)
    print_cm(cm1)
    
    print("-"*20)
    print(f"f-score AQI*  : {f1_score(aqi2_true, aqi2_pred, average='micro')}")
    cm2 = confusion_matrix(aqi2_true, aqi2_pred, labels=labels)
    print_cm(cm2)




