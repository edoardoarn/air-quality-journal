# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot as plt

from src.metrics import smape_score, mae_score, rmse_score
from src.commons import decay_weights


# %%
DATA_PATH = "/mnt/data1/romualdo_data/datasets/air-quality/processed/global"
DATA_NAME = "data_cleaned_cycle_h_rbf_w24s1.csv"
PLOT_PATH = os.path.join("../../plots/notebooks/train_baselines", DATA_NAME.replace(".csv", ""))
MODEL_PATH = os.path.join("../../models", DATA_NAME.replace(".csv", ""))
RESULT_PATH = os.path.join("../../results", DATA_NAME.replace(".csv", ""))
TRAIN_RATIO = 0.8
TARGET_VARS = ["pm10","pm25","no2","co","c6h6","nox","bc","so2","nh3","o3"]
SEED = 42
PATHS = [MODEL_PATH, PLOT_PATH, RESULT_PATH]


# %%
for p in PATHS: 
    if not os.path.exists(p):
        os.makedirs(p)


# %%
data = pd.read_csv(os.path.join(DATA_PATH, DATA_NAME), index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
data.dropna(inplace=True)
assert data.isna().sum().sum() == 0
data


# %%
split = int(np.floor(len(data) * TRAIN_RATIO))
X_features = [f for f in data.columns if f not in TARGET_VARS]
Y_features = [f for f in data.columns if f in TARGET_VARS]

train_data, test_data = data.iloc[:split], data.iloc[split:]
X_train_df, Y_train_df = train_data[X_features], train_data[Y_features]
X_test_df, Y_test_df = test_data[X_features], test_data[Y_features]
print(X_train_df.shape, X_test_df.shape, Y_train_df.shape, Y_test_df.shape)


# %%
from sklearn.linear_model import LinearRegression, BayesianRidge, HuberRegressor
from sklearn.svm import LinearSVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from joblib import dump, load


# %%
from sklearn.metrics import r2_score

metrics = [mae_score, rmse_score, r2_score, smape_score]


# %%
scaler = StandardScaler().fit(X_train_df)
transformer = FunctionTransformer(func=np.log1p, inverse_func=np.expm1)
X_train = scaler.transform(X_train_df)
X_test = scaler.transform(X_test_df)
Y_train, Y_test = Y_train_df.values, Y_test_df.values


# %%
weights = decay_weights(np.arange(len(X_train)), 0.5)
plt.plot(X_train_df.index, weights)


# %%
# for each output
weights = decay_weights(np.arange(len(X_train)), 0.5)
feature_names = Y_train_df.columns
results = []

for i in range(Y_train.shape[1]):
    feature = feature_names[i]
    #model definition inside for reinitialization 
    models = {
        "LinReg": LinearRegression(), 
        "BRidge": BayesianRidge(n_iter=1000, tol=1e-4),
        "HuberR": HuberRegressor(max_iter=1000),
        "LinSVR": LinearSVR(tol=1e-4, C=0.9, random_state=SEED),
        "RF-n200-d6": RandomForestRegressor(n_estimators=200, n_jobs=4, max_depth=6, random_state=SEED,),
        "AB-n200-d6": AdaBoostRegressor(DecisionTreeRegressor(max_depth=6), n_estimators=200, random_state=SEED)
    }
    print("-"*20)
    print(f"fitting on {feature}...")
    print("-"*20)
    for m_name, model in models.items():
        y_true = Y_test[:,i]
        reg = model.fit(X_train, transformer.transform(Y_train[:,i].reshape(-1, 1)), sample_weight=weights)
        y_pred = reg.predict(X_test).reshape(-1, 1)
        y_pred = transformer.inverse_transform(y_pred).flatten()

        model_path = ""
        model_path = os.path.join(MODEL_PATH, feature)
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        model_path = os.path.join(model_path,  f"{m_name}.jlib")
        dump(reg, model_path)

        f = plt.figure(figsize=(50, 6))
        plt.plot(Y_test_df.index, y_true, 'g--')
        plt.plot(Y_test_df.index, y_pred, 'r-')
        plt.title(f"{type(model).__name__}")
        plt.tight_layout()
        plot_name = f"{feature}_{m_name}.png"
        plt.savefig(os.path.join(PLOT_PATH, plot_name))
        plt.close(f)

        scores = [f(y_true, y_pred) for f in metrics]
        for m, score in zip(metrics, scores):
            metric_name = m.__name__
            name = f"{m_name} - {metric_name}"
            print(f"{name:<40s}: {score:04.4f}")
            results.append([feature, m_name, metric_name, score])
        print("-"*20)

results_df = pd.DataFrame(results, columns=["feature", "model", "metric", "score"])    


# %%
results_df.to_csv(os.path.join(RESULT_PATH, "run01.csv"), index=False)

