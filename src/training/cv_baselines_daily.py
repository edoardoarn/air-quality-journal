import os
import argparse
import numpy as np
import pandas as pd
import glob
from datetime import datetime
from matplotlib import pyplot as plt

from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
from sklearn.svm import LinearSVR
from sklearn.linear_model import LinearRegression, BayesianRidge, HuberRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score
from joblib import dump, load

from src.commons import nested_cv_splits, equal_splits, decay_weights
from src.metrics import mae_score, rmse_score, smape_score
from src.config import TrainingConfig


class DailyConfig(TrainingConfig):
    """Daily override for the baisc training config
    """
    SEED = 42
    DATA_NAME = "daily_cleaned_cycle_h_w48s1_rbf.csv"    

    def __init__(self, args):
        super().__init__(args)
        self.TARGET_VARS = ["pm10","pm25","no2","co","c6h6","nox","bc","so2","nh3","o3"]
        self.DROP = ["no2","co","c6h6","nox","bc","so2","nh3","o3"]


def check_paths(config: TrainingConfig):
    for p in config.PATHS: 
        if not os.path.exists(p):
            os.makedirs(p)


def identity(x):
    return x


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cross Validation loop for scikit learn models on daily data.")
    parser.add_argument("--data", "-d", type=str, default="daily_cleaned_cycle_h_rbf_w3s1.csv")
    parser.add_argument("--plot_results", "-p", action="store_true", default=False)
    parser.add_argument("--save_results", "-s", action="store_true", default=False)
    args = parser.parse_args()
    date = datetime.now().strftime("%d-%m-%Y_%H-%M")

    # load config, data and drop nulls derived from shifts
    config = DailyConfig(args)
    #check_paths(config)
    for path in config.PATHS:
        assert os.path.exists(path) and os.path.isdir(path), f"Missing folder: {path}"

    data = pd.read_csv(config.DATA_PATH, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    data.dropna(inplace=True)
    assert data.isna().sum().sum() == 0, "Null values inside"

    # divide features from target variables
    featuresX = [f for f in data.columns if f not in config.TARGET_VARS]
    featuresY = [f for f in data.columns if f in config.TARGET_VARS]
    featuresY = [f for f in featuresY if f not in config.DROP]

    engineered = [f for f in featuresX if f.startswith("rbf_") or "_sin" in f or "_cos" in f or f == "holiday"]
    transformable = [f for f in featuresX if f not in engineered]
    dataX, dataY = data[featuresX], data[featuresY]
    max_val = dataY.max()

    # load scaler, log transformer and metrics, scale data
    scaler = MinMaxScaler()
    transformer = FunctionTransformer(func=np.log1p, inverse_func=np.expm1, validate=False)
    
    metrics = [mae_score, rmse_score, r2_score, smape_score]
    dataX = pd.DataFrame(index=dataX.index, columns=dataX.columns, data=scaler.fit_transform(dataX))
    results = []

    # start iterating through folds
    for k, (X_trainDF, X_valDF, X_testDF, Y_trainDF, Y_valDF, Y_testDF) in enumerate(equal_splits(dataX, dataY, periods=3)):
        # listen, performance sucks, I just wanted to add something useful to the training
        # we are not using the val set anyway
        X_trainDF = X_trainDF.append(X_valDF)
        Y_trainDF = Y_trainDF.append(Y_valDF)

        X_train, X_test = X_trainDF.values, X_testDF.values
        Y_train, Y_test = Y_trainDF.values, Y_testDF.values

        # a = pd.concat([X_trainDF, Y_trainDF], axis=1)
        # b = pd.concat([X_testDF, Y_testDF], axis=1)

        # assert a.shape[1] == (X_trainDF.shape[1] + Y_trainDF.shape[1])
        # assert b.shape[1] == (X_testDF.shape[1] + Y_testDF.shape[1])
        # assert np.all(np.isfinite(a))
        # assert np.all(np.isfinite(b))
        # a.to_csv(f"{config.DATA_NAME.replace('.csv', '')}_train_{k}.csv")
        # b.to_csv(f"{config.DATA_NAME.replace('.csv', '')}_test_{k}.csv")
        # continue

        print(f"Training fold #{k:2d}")
        print(X_train.shape, X_test.shape)

        assert np.all(np.isfinite(X_train)), "NaNs in train data"
        assert np.all(np.isfinite(X_test)), "NaNs in test data"
        print(featuresY)

        # iterate through target vars
        for i, feature in enumerate(featuresY):
            print("-"*20)
            print(f"fitting on {feature}...")
            print("-"*20)            
            models = {
                #"BRidge": BayesianRidge(n_iter=500, tol=1e-3),
                "RF-n100-nd":RandomForestRegressor(n_estimators=100, n_jobs=10),
            }
            # iterate models
            for model_name, model in models.items():
                y_true = Y_test[:,i]
                reg = model.fit(X_train, transformer.transform(Y_train[:,i].reshape(-1, 1)).ravel())
                y_pred = reg.predict(X_test).reshape(-1, 1)
                y_pred = transformer.inverse_transform(y_pred).flatten()
                y_pred = np.clip(y_pred, y_pred.min(), max_val[i])

                scores = [f(y_true, y_pred) for f in metrics]
                for m, score in zip(metrics, scores):
                    metric_name = m.__name__
                    name = f"{model_name} - {metric_name}"
                    print(f"{name:<40s}: {score:04.4f}")
                    results.append([k, feature, model_name, metric_name, score])
                print("-"*20)

                # plot regression results
                if args.plot_results:
                    fig = plt.figure(figsize=(50, 6))
                    plt.plot(X_testDF.index, y_true, color="silver", linestyle="--")
                    plt.plot(X_testDF.index, y_pred, color="blue")
                    plt.tight_layout()
                    plt.title(model_name)
                    plt.savefig(os.path.join(config.PLOT_PATH, f"{date}_{feature}_{model_name}_{k}.png"))
                    plt.close(fig)

                # compute bins if the pollutant serves for AQI
                if args.save_results:
                    aqi_file = os.path.join(config.AQI_PATH, f"{date}_{model_name}_{feature}_minmax.csv")
                    mode = "a" if os.path.exists(aqi_file) else "w"
                    keep_header = True if mode == "w" else False
                    aqi = pd.DataFrame(index=X_testDF.index, data=np.column_stack((y_true, y_pred)), columns=["y_true", "y_pred"])
                    aqi.to_csv(aqi_file, mode=mode, header=keep_header)
    exit(0)
    result_name = f"cv_run_{date}.csv"
    # save new run
    results_df = pd.DataFrame(results, columns=["fold", "feature", "model", "metric", "score"])    
    results_df.to_csv(os.path.join(config.RESULT_PATH, result_name), index=False)




