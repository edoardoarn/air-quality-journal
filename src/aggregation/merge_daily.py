import pandas as pd
import numpy as np
import os
from matplotlib import pyplot as plt

DATA_PATH = '/mnt/data1/romualdo_data/datasets/air-quality/processed'
OUT_PATH = '/mnt/data1/romualdo_data/datasets/air-quality/processed/global'

WTR_PATH = 'weather'
TRF_PATH = 'transits'
POL_PATH = 'pollutants'

DATA_NAMES = ['data_imputed.csv', 'data_imputed.csv', 'data_averaged.csv']


def parse_date(text):
    return pd.to_datetime(np.datetime64(text))


if __name__ == "__main__":
    weather = pd.read_csv(os.path.join(DATA_PATH, WTR_PATH, DATA_NAMES[0]), index_col=0, parse_dates=True, date_parser=parse_date)
    traffic = pd.read_csv(os.path.join(DATA_PATH, TRF_PATH, DATA_NAMES[1]), index_col=0, parse_dates=True, date_parser=parse_date)
    pollution = pd.read_csv(os.path.join(DATA_PATH, POL_PATH, DATA_NAMES[2]), index_col=0, parse_dates=True, date_parser=parse_date)

    print(pollution.isna().sum())

    pollution = pollution.drop(columns=[c for c in pollution.columns if c not in ["pm10", "pm25"]])
    daily_pollution = pollution.groupby(pollution.index.date).mean()
    daily_pollution = daily_pollution.interpolate(method="linear", limit_direction="both")

    # for c in interp.columns:
    #     f = plt.figure(figsize=(60, 10))
    #     plt.plot(interp.index, interp[c])
    #     plt.savefig(f"{c}.png")
    #     plt.close(f)

    daily_weather = weather.groupby(weather.index.date).mean()
    daily_traffic = traffic.groupby(traffic.index.date).mean()

    wmin, wmax = daily_weather.index.min(), daily_weather.index.max()
    pmin, pmax = daily_pollution.index.min(), daily_pollution.index.max()
    tmin, tmax = daily_traffic.index.min(), daily_traffic.index.max()

    start, end = max(wmin, pmin, tmin), min(wmax, pmax, tmax)
    print(f"starting from {start} till {end}")

    assert all(daily_weather.notna()), "There are NaNs in your weather man"
    assert all(daily_traffic.notna()), "There are NaNs in your traffic man"
    assert all(daily_pollution.notna()), "There are NaNs in your pollution man"

    dates = pd.date_range(start, end)
    final = daily_weather.loc[dates].join(daily_traffic.loc[dates]).join(daily_pollution.loc[dates])
    assert all(final.notna()), "There are NaNs in your merged set man"

    print(final.shape)
    print(final.head())
    final.to_csv(os.path.join(OUT_PATH, "daily.csv"))
    print("done!")

