import pandas as pd
import numpy as np
import os

DATA_PATH = 'D:\\Datasets\\air-quality\\processed'
OUT_PATH = 'D:\\Datasets\\air-quality\\processed\\global'

WTR_PATH = 'weather'
TRF_PATH = 'transits'
POL_PATH = 'pollutants'

DATA_NAMES = ['data_imputed.csv', 'data_imputed.csv', 'data_averaged.csv']


def parse_date(text):
    return pd.to_datetime(np.datetime64(text))


if __name__ == "__main__":
    # ---------------------importing files and checking result-----------------------
    weather = pd.read_csv(os.path.join(DATA_PATH, WTR_PATH, DATA_NAME), index_col=0, parse_dates=True, date_parser=parse_date)
    pollution = pd.read_csv(os.path.join(DATA_PATH, POL_PATH, DATA_NAME), index_col=0, parse_dates=True, date_parser=parse_date)
    traffic = pd.read_csv(os.path.join(DATA_PATH, TRF_PATH, DATA_NAME), index_col=0, parse_dates=True, date_parser=parse_date)
    dfs = [weather, traffic, pollution]

    # first row is missing from pollution, traffic is excluding the last two months because of missing data
    # we keep from 2013-01-01 00:00 to 2015-10-24 00:00
    str_idx = pd.to_datetime(np.datetime64("2013-01-01 00:00:00"))
    end_idx = pd.to_datetime(np.datetime64("2015-10-24 00:00:00"))
    general_index = pd.date_range(str_idx, end_idx, freq="H")

    print('weather_rows:', weather.shape[0], 'air_rows:',
          pollution.shape[0], 'traffic_rows:', traffic.shape[0])
    print(min(weather.index), max(weather.index))
    print(min(pollution.index), max(pollution.index))
    print(min(traffic.index), max(traffic.index))
    for df in dfs:
        print(df.isna().sum())

    # weathre has wind speed with too many missing vals and pressure missing first two hours
    print("Refining weather...")
    a = weather[weather.index <= end_idx]
    print(a[a.pressure.isna()])
    print(a.head(5))
    print("-"*20)

    print("Refining pollution...")
    tmp = pollution[pollution.index <= end_idx]
    c = traffic[traffic.index <= end_idx]
    # first row of pollution is missing, extrapolate
    b = pd.DataFrame(columns=tmp.columns, index=a.index)
    b.loc[tmp.index] = tmp
    print(b.head(5))
    print("-"*20)

    for col in b.columns:
        b[col] = b[col].astype(np.float64)
    b = b.interpolate(method="linear", limit_direction="backward", axis=0)
    print(b.head(5))

    print("Checking consistency...")
    length = len(general_index)
    for df in [a, b, c]:
        assert len(df.index) == length
        assert np.all(df.isna().sum() == 0)
        assert min(df.index) == str_idx
        assert max(df.index) == end_idx

    # merging measurements per hour
    print("-"*20)
    print("Merging...")
    complete = a.join(b).join(c)
    print(complete.head(2))

    print(complete.shape)
    assert len(complete) == len(general_index)
    assert complete.isna().sum().sum() == 0
    # ------------------------output on file-----------------------------------------
    print("Saving final csv...")
    complete.to_csv(os.path.join(OUT_PATH, "data.csv"))
    print("Done!")