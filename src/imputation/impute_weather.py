import os
import argparse
import numpy as np 
import pandas as pd

from matplotlib import pyplot as plt
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer


DEFAULT_DATA_PATH = "D:\\Datasets\\air-quality\\processed\\weather\\data_averaged.csv"
DEFAULT_OUTPUT_PATH = "D:\\Datasets\\air-quality\\processed\\weather\\data_imputed.csv"
SEED = 42


def find_missing_periods(df, feature):
    periods = []
    dates = df[df[feature].notna()].index
    diff = np.diff(dates) # a[i+1] - a[i]
    indices = np.argwhere(diff > np.timedelta64(1, 'h')) # these are the i-th index in dates, needs also i+1
    for i in indices:
        periods.append(tuple([dates[i], dates[i+1]]))
    return periods


def plot_with_nans(series, title="", ax=None):
    xx = series.index
    yy = series
    xx_missing = xx[series.isna()]
    yy_missing = np.ones(len(xx_missing)) * yy.mean()
    if not ax:
        plt.plot(xx, yy)
        plt.title(title)
        plt.scatter(xx_missing, yy_missing, c='r')
    else:
        ax.plot(xx, yy)
        ax.set_title(title)
        ax.scatter(xx_missing, yy_missing, c='r')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Takes the given averaged dataset and imputes columns")
    parser.add_argument("--path", "-p", type=str, default=DEFAULT_DATA_PATH)
    parser.add_argument("--out", "-o", type=str, default=DEFAULT_OUTPUT_PATH)
    args = parser.parse_args()

    # load data
    data = pd.read_csv(args.path, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))

    # count missing
    nulls = data.isna().sum()
    incomplete_features = []
    for feature, count in zip (data.columns, nulls):
        percent = count/len(data) * 100.0
        print(f"{feature:<10s}: {count:<6d} - {percent:.4f}%")
        if (count > 0):
            incomplete_features.append((feature, percent))
    
    # drop everything over 10% missing (only wind direction should)
    threshold = 10
    dropped = [f for f, p in incomplete_features if p > threshold]
    data = data.drop(columns=dropped)

    # init imputer
    imputer = IterativeImputer(max_iter=50, imputation_order="ascending", verbose=1, random_state=SEED)
    imputed = imputer.fit_transform(data.values)
    imputed = pd.DataFrame(index=data.index, columns=data.columns, data=imputed)

    # shoe some plots for visual check
    for col in imputed.columns:
        plt.plot(imputed.index, imputed[col])
        plt.show()
    # assert no data is missing
    print(imputed.isna().sum())

    # save to file
    imputed.to_csv(args.out, index=True)