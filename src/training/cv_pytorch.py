import os
import argparse
import glob
import pandas as pd
import numpy as np
from datetime import datetime
from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
from sklearn.metrics import r2_score
from matplotlib import pyplot as plt

import torch
from torch.utils.data import DataLoader
from torch.optim import Adam, SGD, rmsprop
from torch.nn import MSELoss
from beacon.base import StatefulTrainer, StatelessTrainer, TrainerState
from beacon.models.rnn import LSTMRegressor, StateInit
from beacon.data import SlidingWindowDataset
from beacon import metrics as bm
from beacon.callbacks import PeriodicCheckpoint, MetricsLogger

from src.metrics import smape_score, mae_score, rmse_score
from src.commons import decay_weights, equal_splits
from src.config import TorchConfig


def check_paths(config: TorchConfig):
    for p in config.PATHS: 
        if not os.path.exists(p):
            os.makedirs(p)

def identity(x):
    return x


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Cross Validation loop for scikit learn models.")
    parser.add_argument("--data", "-d", type=str, default="data_cleaned_cycle_h_rbf.csv")
    parser.add_argument("--device", "-e", type=str, default="cpu")
    parser.add_argument("--plot_results", "-p", action="store_true", default=False)
    parser.add_argument("--save_results", "-i", action="store_true", default=False)
    args = parser.parse_args()
    date = datetime.now().strftime("%d-%m-%Y_%H-%M")

    # load config, data and drop nulls derived from shifts
    config = TorchConfig(args)
    check_paths(config)
    for path in config.PATHS:
        assert os.path.exists(path) and os.path.isdir(path), f"Missing folder: {path}"
    
    data = pd.read_csv(config.DATA_PATH, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    data.dropna(inplace=True)
    assert data.isna().sum().sum() == 0, "Null values inside"

    # divide features from target variables
    featuresX = [f for f in data.columns if f not in config.TARGET_VARS]
    featuresY = [f for f in data.columns if f in config.TARGET_VARS]
    featuresY = [f for f in featuresY if f not in config.DROP]
    engineered = [f for f in featuresX if f.startswith("rbf_") or "_sin" in f or "_cos" in f or f == "holiday"]
    transformable = [f for f in featuresX if f not in engineered]
    dataX, dataY = data[featuresX], data[featuresY]
    max_val = dataY.max()
    print(dataY.describe())
    exit(0)

    # load scaler, log transformer and metrics, scale data
    scaler = StandardScaler()
    transformer = FunctionTransformer(func=np.log1p, inverse_func=np.expm1, validate=False)
    
    metrics = [mae_score, rmse_score, r2_score, smape_score]
    dataX = pd.DataFrame(index=dataX.index, columns=dataX.columns, data=scaler.fit_transform(dataX))
    results = []

    # iterate over 3 periods as CV
    for k, (X_trainDF, X_valDF, X_testDF, Y_trainDF, Y_valDF, Y_testDF) in enumerate(equal_splits(dataX, dataY, periods=3)):
        X_train, X_valid, X_test = X_trainDF.values, X_valDF.values, X_testDF.values
        Y_train, Y_valid, Y_test = Y_trainDF.values, Y_valDF.values, Y_testDF.values
        # iterate over target features
        for t, target in enumerate(featuresY):
            # prepare data loaders
            train_loader = DataLoader(SlidingWindowDataset(X_train, transformer.transform(Y_train[:,t].reshape(-1, 1)), window=config.WINDOW), 
                            batch_size=config.BATCH_SIZE, 
                            shuffle=not config.STATEFUL, 
                            drop_last=config.STATEFUL)
            valid_loader = DataLoader(SlidingWindowDataset(X_valid, transformer.transform(Y_valid[:,t].reshape(-1, 1)), window=config.WINDOW), 
                            batch_size=config.BATCH_SIZE, 
                            drop_last=config.STATEFUL)             
            test_loader = DataLoader(SlidingWindowDataset(X_test, transformer.transform(Y_test[:,t].reshape(-1, 1)), window=config.WINDOW), 
                            batch_size=config.BATCH_SIZE, 
                            drop_last=config.STATEFUL)

            nh = config.N_HIDDEN
            nl = config.N_LAYERS
            wn = config.WINDOW
            model = LSTMRegressor(
                input_dim=X_train.shape[1], 
                hidden=config.N_HIDDEN, 
                layers=config.N_LAYERS, 
                classes=1, 
                dropout=config.DROPOUT, 
                state_init=StateInit.RAND)
            model_name = str(type(model).__name__).lower().replace("regressor", "")
            model_name = f"{model_name}_{'stateful' if config.STATEFUL else 'stateless'}_{nh}x{nl}_w{wn}"
            # prepare trainer and model
            trainer_cls = StatefulTrainer if config.STATEFUL else StatelessTrainer
            trainer = trainer_cls(
                model=model, 
                criterion=MSELoss(), 
                optimizer=Adam(model.parameters(), lr=config.LR),
                metrics={"mse": bm.mse},
                epochs=config.EPOCHS)
            final_path = os.path.join(config.MODEL_PATH, target)
            logger_path = os.path.join(config.LOGS_PATH, f"{date}_{model_name}_{target}_{k}.log")
            trainer = trainer.to(config.DEVICE).register(MetricsLogger(out=logger_path))

            # fit model and predict
            trainer = trainer.fit(train_loader, validation=valid_loader)
            y_pred, (y_true, _) = trainer.evaluate(test_loader)
            y_pred = transformer.inverse_transform(y_pred.cpu().detach().numpy())
            y_true = transformer.inverse_transform(y_true.cpu().detach().numpy())

            # get datetime index from original DF, considering both batches and lstm windows
            limit = len(X_testDF)
            if config.STATEFUL:
                limit -= len(Y_test) % config.BATCH_SIZE
            xx = X_testDF.index.values[wn:limit]
            # compute and store scores for each fold
            scores = [f(y_true, y_pred) for f in metrics]
            for m, score in zip(metrics, scores):
                metric_name = m.__name__
                name = f"{model_name} - {metric_name}"
                print(f"{name:<40s}: {score:04.4f}")
                results.append([k, target, model_name, metric_name, score])
            print("-"*20)

            # compute bins if the pollutant serves for AQI
            if args.save_results:
                aqi_file = os.path.join(config.AQI_PATH, f"{date}_{model_name}_{target}.csv")
                mode = "a" if os.path.exists(aqi_file) else "w"
                keep_header = True if mode == "w" else False
                aqi = pd.DataFrame(index=xx, data=np.column_stack((y_true, y_pred)), columns=["y_true", "y_pred"])
                aqi.to_csv(aqi_file, mode=mode, header=keep_header)
            
            # plot regression results
            if args.plot_results:
                fig = plt.figure(figsize=(50, 6))
                plt.plot(xx, y_true, color="silver", linestyle="--")
                plt.plot(xx, y_pred, color="blue")
                plt.tight_layout()
                plt.title(model_name)
                plt.savefig(os.path.join(config.PLOT_PATH, f"{date}_{target}_{model_name}_{k}.png"))
                plt.close(fig)

 # get previous runs
    current_results = glob.glob(os.path.join(config.RESULT_PATH, "*.csv"))
    current_results = [os.path.basename(f) for f in current_results if os.path.basename(f).startswith("cv_pt_")]
    result_name = "cv_pt_{}_{}_{:02d}.csv"
    # update count
    current_run = 1
    if len(current_results) > 0:
        current_run = max([int(f.replace(".csv", "").split("_")[-1]) for f in current_results]) + 1
    result_name = result_name.format(date, model_name, max(1, current_run))
    # save new run
    results_df = pd.DataFrame(results, columns=["fold", "feature", "model", "metric", "score"])    
    results_df.to_csv(os.path.join(config.RESULT_PATH, result_name), index=False)



