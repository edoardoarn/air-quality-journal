import pandas as pd
import numpy as np
import datetime
import os
import argparse


DATA_PATH = '/mnt/data1/romualdo_data/datasets/air-quality/processed/global/daily_cleaned_cycle_h_rbf.csv'
HOLIDAYS = '/mnt/data1/romualdo_data/datasets/air-quality/holidays.txt'
ACTION = ""
TIME_FEATURES = ["month", "weekday"]
TARGET_VARS = ["pm10","pm25","no2","co","c6h6","nox","bc","so2","nh3","o3"]
DEFAULT_DROP = ["na_euro","na_fuel","na_vehicle","bus","cargo","people","residents",
                "non_residents","authorized","non_authorized","service","other_vehicles"]
WINDOW = 2
MODS = {"y": 365, "m": 30, "w": 7}


# parse text as date
def parse(text : str, fmt="%Y/%m/%d"):
    return pd.to_datetime(text, format=fmt)


# read file containing dates
def read_dates(path : str):
    dates = []
    with open(path) as f:
        for line in f.readlines():
            dates.append(parse(line.strip()).date())
    return dates


# check if sunday or in holiday list
def is_holiday(date : pd.Timestamp, holidays : list):
    day = date.date()
    # weekofday goes from 0 to 6
    return date.dayofweek == 6 or day in holidays


def rbf(x, m, mod, alpha=0.2):
    k = -1/(2*alpha)
    return np.exp(k * (x % mod - m)**2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Augment data with daily resolution")
    parser.add_argument("-d", "--data", action="store", default=DATA_PATH)
    parser.add_argument("-a", "--action", action="store", default=ACTION)
    parser.add_argument("-w", "--window", action="store", type=int, default=WINDOW)
    parser.add_argument("-s", "--step", action="store", type=int, default=1)
    parser.add_argument("-c", "--cols", action="store")
    parser.add_argument("--mod", "-m", action="store", type=str, default="y")

    args = parser.parse_args()
    print(vars(args))

    print("reading data...")
    use_columns = [] if args.cols is None else args.cols.split()
    data = pd.read_csv(args.data, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    path = os.path.dirname(args.data)
    name = os.path.basename(args.data)

    ###################################################
    # range_clip
    # find min and max of each series and clip vals
    ###################################################
    if args.action == "range_clip":
        minmax = data.describe().loc[['min', 'max', '75%']]
        for col in minmax.columns:
            v = minmax[col]
            print(f"{col:<20s} - min: {v.loc['min']:10.3f}, max: {v.loc['max']:10.3f}, 3rd quartile: {v['75%']:10.3f}")
            # manually set any feature to clip and its values
        #data.so2.clip(lower=data.so2.min(), upper=25, inplace=True)
        #data.to_csv(os.path.join(path, "data.csv"))
    
    ###################################################
    # drop
    # drop the given column names
    ################################################### 
    elif args.action == "drop":
        to_drop = use_columns
        to_drop += [c for c in DEFAULT_DROP if c in data.columns]
        print(f"dropping: {to_drop}")
        data.drop(columns=to_drop, inplace=True)
        data.to_csv(os.path.join(path, name.replace(".csv", "_cleaned.csv")))


    ###################################################
    # add_time
    # add time-based features (month, week...)
    ################################################### 
    elif args.action == "time":
        cols = TIME_FEATURES
        if not all(x in data.columns for x in cols):
            print(f"adding: {cols}")
            data[cols[0]] = [d.month for d in  data.index]
            data[cols[1]] = [d.dayofweek for d in data.index]
            name = name.replace(".csv", "_time.csv")
            data.to_csv(os.path.join(path, name))
        else:
            print(f"already present in {args.data}!")
    
    ###################################################
    # cycle
    # encode time-based features (month, week...) with sin
    ################################################### 
    elif args.action == "cycle":
        periods = {"month": (12, 1), "weekday": (7, 0)}

        if not all(x in data.columns for x in periods.keys()):
            raise AssertionError("Add time features first!")

        for col, (period, sub) in periods.items():
            data[f"{col}_sin"] = np.sin((data[col] - sub) * (2.0 * np.pi / period))
            data[f"{col}_cos"] = np.cos((data[col] - sub) * (2.0 * np.pi / period))

        new_cols = [f"{c}_sin" for c in periods.keys()] + [f"{c}_cos" for c in periods.keys()]
        data.drop(columns=list(periods.keys()), inplace=True)
        print(data[new_cols].head())
        name = name.replace("_time", "_cycle")
        data.to_csv(os.path.join(path, name))
    
    ###################################################
    # holidays
    # add holiday column with bool values = true when the day is a holiday
    ################################################### 
    elif args.action == "holidays":
        print("adding holiday bool column...")
        holidays = read_dates(HOLIDAYS)
        data['holiday'] = [1 if is_holiday(d, holidays) else 0 for d in data.index]
        print("holidays and sundays: ", data.holiday.sum())
        name = name.replace(".csv", "_h.csv")
        data.to_csv(os.path.join(path, name))
    
    ###################################################
    # rbf
    # add a RBF curve for every n periods
    ###################################################
    elif args.action == "rbf":
        from matplotlib import pyplot as plt

        x = np.arange(len(data))
        print(len(data))
        f = plt.figure(figsize=(30, 5))
        for month in range(12):
            m = (30 * month + 15)
            print(month+1)
            feature = rbf(x, m, 365, alpha=10e1)
            plt.plot(x, feature)
            f_name = f"rbf_{month+1:02d}"
            data[f_name] = feature
        
        plt.savefig("test.png")
        name = name.replace(".csv", "_rbf.csv")
        data.to_csv(os.path.join(path, name))
    
    ###################################################
    # shift
    # add columns for lagged features, shifting the current cols
    ###################################################
    elif args.action == "shift":
        if args.window == 1:
            print("window size is 1! Set a value equals to days back + current (> 1)")
            exit(0)
        cols = [d for d in data.columns if d not in TARGET_VARS]
        print(f"shifting: {','.join(cols)}")
        for i in range(1, args.window):
            s = i * args.step
            print(f"shifting by t-{s}...")
            # shift only predictor variables
            for col in cols:
                newcol = f"{col}-{s}"
                data[newcol] = data[col].shift(s)
        name = name.replace(".csv", f"_w{args.window}s{args.step}.csv")
        print(f"done! saving to: {name}")
        data.to_csv(os.path.join(path, name))

    print("done!")
