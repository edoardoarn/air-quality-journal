import pandas as pd
import numpy as np
import argparse
import glob
import os
from collections import defaultdict


DEFAULT_DATA_PATH = "D:/Datasets/air-quality/processed/weather/data_merged.csv"
DEFAULT_OUTPUT_PATH = "D:/Datasets/air-quality/processed/weather"


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Converts the many CSVs into a single file")
    parser.add_argument("--path", "-p", type=str, default=DEFAULT_DATA_PATH)
    parser.add_argument("--out", "-o", type=str, default=DEFAULT_OUTPUT_PATH)
    args = parser.parse_args()

    # read merged data
    data = pd.read_csv(args.path, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    print(data.isna().sum())

    groups = defaultdict(list)
    for c in data.columns:
        p = c.split("_")
        key = "_".join(p[:-1])
        val = p[-1]
        groups[key].append(val)

    # create the empty dataframe for averages
    averaged = pd.DataFrame(index=data.index, columns=list(groups.keys()))

    # fill in data
    for name, sensors in groups.items():
        cols = [f"{name}_{s}" for s in sensors]
        mean = data[cols].mean(axis=1)
        averaged.loc[mean.index, name] = mean.values
    
    # print info on the result
    print(averaged.shape)
    print(averaged.isna().sum())
    print(averaged.head(2))

    # save to output folder
    #averaged.to_csv(os.path.join(args.out, "data_averaged.csv"), index=True)