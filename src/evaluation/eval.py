import os
import argparse
import pandas as pd
import numpy as np
import glob
from matplotlib import pyplot as plt

from src.config import EvalConfig


config = EvalConfig(argparse.Namespace(data="data_cleaned_cycle_h_rbf_w24s1"))
pretty_names = {
    "model": "model",
    "mae_score": "MAE",
    "rmse_score": "RMSE",
    "r2_score": "R^2",
    "smape_score": "sMAPE"
}


def print_latex(pollutant, table):
    columns = table.columns.values
    print("\hline")
    print(f"\multicolumn{{{len(columns)}}}{{|c|}}{{{pollutant.upper()}}}")
    print(f"{' & '.join([pretty_names[c] for c in columns])} \\\\")
    print("\hline")
    for idx, row in table.iterrows():
        vals = " & ".join([f"{v:.2f}" if type(v) == float else v for v in row])
        print(f"{vals} \\\\")
    print("\hline")





if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate results generated as csv")
    parser.add_argument("--path", "-p", type=str, default=config.DATA_PATH)
    parser.add_argument("--what", type=str, default="logs", choices=["logs", "results"])
    parser.add_argument("--format", "-f", type=str, default="latex")

    args = parser.parse_args()
    crossval = "cv_" in args.path.split("/")[-1]

    if args.what == "results":
        data = pd.read_csv(args.path)
        features = data.feature.unique()
        models = data.model.unique()
        metrics = data.metric.unique()
        if crossval:
            folds = data.fold.unique()
        
        for pollutant in features:
            results = data[data.feature == pollutant].drop(columns=["feature", "fold"])
            results = results.groupby(["model", "metric"]).mean()
            pivot = results.pivot_table(columns="metric", index="model", values="score").reset_index()

            if args.format == "latex":
                print_latex(pollutant, pivot)
            else:
                print(pollutant)
                print(pivot)
            print("")
    elif args.what == "logs":
        log_file = args.path if ".log" in args.path else sorted(glob.glob(os.path.join(config.LOGS_PATH, "*.log")))[-1]
        print(f"plotting {log_file}...")
        df = pd.read_csv(log_file, index_col="epoch")
        df = df.iloc[1:]
        f = plt.figure(figsize=(10, 10))

        for c in df.columns:
            plt.plot(df.index, df[c], label=c)
        plt.title(log_file)
        plt.legend()
        plt.savefig(os.path.join(config.LOGS_PATH, os.path.basename(log_file).replace(".log", ".png")))