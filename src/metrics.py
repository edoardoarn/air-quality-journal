import numpy as np


def smape_score(y_true, y_pred):
    """Computes the symmetric Mean Absolute Percentage Error on the given numpy arrays.
    :param y_true:  list of actual values
    :type y_true:   numpy array of size (N,)
    :param y_pred:  list of predicted values
    :type y_pred:   numpy array of size (N,)
    :return:        symmetric MAPE, scaled bwtween 0 (no error) and 1 (absolute chaos)
    :rtype:         float64
    """
    return np.mean(np.abs(y_true - y_pred) / (np.abs(y_true) + np.abs(y_pred)))


def mae_score(y_true, y_pred):
    """Computes the mean absolute error over the given arrays.
    :param y_true:  list of actual values
    :type y_true:   numpy array of size (N,)
    :param y_pred:  list of predicted values
    :type y_pred:   numpy array of size (N,)
    :return:        mean absolute error (mean of absolute differences between the values)
    :rtype:         float64
    """
    return np.mean(np.abs(y_true - y_pred))


def rmse_score(y_true, y_pred):
    """Computes the root mean squared error on the given data.

    :param y_true:  list of actual values
    :type y_true:   numpy array of size (N,)
    :param y_pred:  list of predicted values
    :type y_pred:   numpy array of size (N,)
    :return:        root mean squared error
    :rtype:         float64
    """
    return np.sqrt(np.mean((y_true - y_pred)**2))