import os
import argparse
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot scatterplots of the results.")
    parser.add_argument("--data", "-d", type=str, required=True)
    parser.add_argument("--out", "-o", type=str, default=".")
    parser.add_argument("--title", type=str, default=None)
    args = parser.parse_args()

    df = pd.read_csv(args.data, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
    assert "y_true" in df.columns and "y_pred" in df.columns, "true and pred columns missing!" 
    
    filename = os.path.basename(args.data).replace(".csv", "").replace(":", "-")
    title = filename if not args.title else args.title
    xlims = [df.y_true.min(), df.y_true.max()]
    ylims = [df.y_pred.min(), df.y_pred.max()]

    f = plt.figure(figsize=(8, 8))
    plt.scatter(df.y_true, np.clip(df.y_pred, 0, df.y_pred.max()), marker="o", c="k", alpha=0.3)
    plt.title(title)
    plt.xlim(xlims)
    plt.xlabel("actual")
    plt.ylim(xlims)
    plt.ylabel("predicted")
    plt.tight_layout()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.savefig(os.path.join(args.out, f"{filename}.png"))
    plt.close(f)