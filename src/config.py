import os
import numpy as np


class BaseConfig:

    def __init__(self, args):
        assert "data" in args, "Missing data file!"
        basename = args.data.replace(".csv", "")
        self.DATA_NAME = args.data
        self.DATA_DIR = "/mnt/data1/romualdo_data/datasets/air-quality/processed/global"
        self.PROJECT_PATH = "/home/romualdo/projects/_LINKS/air-quality-revenge"
        self.DATA_PATH = os.path.join(self.DATA_DIR, self.DATA_NAME)
        self.PLOT_PATH = os.path.join(self.PROJECT_PATH, "plots", basename)
        self.MODEL_PATH = os.path.join(self.PROJECT_PATH, "models", basename)
        self.RESULT_PATH = os.path.join(self.PROJECT_PATH, "results",basename)
        self.PATHS = [self.MODEL_PATH, self.PLOT_PATH, self.RESULT_PATH]
        self.TARGET_VARS = ["pm10","pm25","no2","co","c6h6","nox","bc","so2","nh3","o3"]
        self.DROP = ["pm10", "pm25", "so2", "nh3"]


class TrainingConfig(BaseConfig):

    def __init__(self, args):
        super().__init__(args)
        basename = args.data.replace(".csv", "")
        self.SEED = 42
        self.LOGS_PATH = os.path.join(self.PROJECT_PATH, "logs",basename)
        self.AQI_PATH = os.path.join(self.PROJECT_PATH, "aqi",basename)
        self.PATHS.append(self.LOGS_PATH)
        self.PATHS.append(self.AQI_PATH)
        self.AQI_LEVELS = {
            "no2": [-1, 50, 100, 200, 400, 1e5],
            "o3":  [-1, 60, 120, 180, 240, 1e5],
            "pm10":[-1, 15, 30, 50, 100, 1e5]
        }


class EvalConfig(TrainingConfig):

    def __init__(self, args):
        super().__init__(args)


class TorchConfig(TrainingConfig):

    def __init__(self, args):
        super().__init__(args)
        self.WINDOW = 1
        self.BATCH_SIZE = 32
        self.N_HIDDEN = 64
        self.N_LAYERS = 2
        self.LR = 1e-2
        self.DROPOUT = 0.5
        self.EPOCHS = 20
        self.STATEFUL = False
        self.DEVICE = "cpu" if "device" not in args else args.device