import os
import argparse
import numpy as np 
import pandas as pd

from matplotlib import pyplot as plt
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer


DEFAULT_DATA_PATH = "D:\\Datasets\\air-quality\\processed\\pollutants\\data_averaged.csv"
DEFAULT_OUTPUT_PATH = "D:\\Datasets\\air-quality\\processed\\pollutants\\data_imputed.csv"
SEED = 42

# weights computed with hourly mean transits
PM_WEIGHTS = np.array([np.nan, np.nan, np.nan, np.nan, 0.16983412,
       0.18748042, 0.33549721, 0.65214485, 0.99785621, 1.30901437,
       1.44976567, 1.50189919, 1.4358604 , 1.32036065, 1.25653023,
       1.26320114, 1.32364134, 1.3444966 , 1.34404836, 1.40920658,
       1.45381273, 1.3209662 , 1.07793764, 0.86349359])


def plot_with_nans(series, title="", ax=None):
    xx = series.index
    yy = series
    xx_missing = xx[series.isna()]
    yy_missing = np.ones(len(xx_missing)) * yy.mean()
    if not ax:
        plt.plot(xx, yy)
        plt.title(title)
        plt.scatter(xx_missing, yy_missing, c='r')
    else:
        ax.plot(xx, yy)
        ax.set_title(title)
        ax.scatter(xx_missing, yy_missing, c='r')


def interpolate_pm(original : pd.DataFrame, daily_values : pd.DataFrame, hourly_weights):
    # get only 23 hours, then interpolate the last one for continuity
    weights = hourly_weights[:-1]
    m = min(original.index)
    for index, row in daily_values.iterrows():
        i = pd.to_datetime(index)
        hourly_idx = pd.date_range(i, i + np.timedelta64(1, "D"), freq="H", closed="left")[:-1]
        available = [i for i, d in enumerate(hourly_idx) if i > 1 and d >= m]
        hourly_pm10 = row.pm10 * weights[available]
        hourly_pm25 = row.pm25 * weights[available]
        original.at[hourly_idx[available], "pm10"] = hourly_pm10
        original.at[hourly_idx[available], "pm25"] = hourly_pm25
    original.pm10 = original.pm10.interpolate(method="linear", limit_direction="both")
    original.pm25 = original.pm25.interpolate(method="linear", limit_direction="both")
    return original


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Takes the given averaged dataset and imputes columns")
    parser.add_argument("--path", "-p", type=str, default=DEFAULT_DATA_PATH)
    parser.add_argument("--out", "-o", type=str, default=DEFAULT_OUTPUT_PATH)
    args = parser.parse_args()

    # load data
    data = pd.read_csv(args.path, index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))

    # fix pm10 and pm25
    # using a smoohted-out version of transits as weights
    daily = data[["pm10", "pm25"]].groupby(data.index.date).mean().interpolate(method="linear", limit_direction="backward")
    interpolated = interpolate_pm(data, daily[:-1], PM_WEIGHTS)
    plt.plot(interpolated.index, interpolated.pm10)
    plt.show()

    # count missing
    nulls = interpolated.isna().sum()
    incomplete_features = []
    for feature, count in zip (interpolated.columns, nulls):
        percent = count/len(interpolated) * 100.0
        print(f"{feature:<10s}: {count:<6d} - {percent:.4f}%")
        if (count > 0):
            incomplete_features.append((feature, percent))

    # drop everything over 10% missing (only wind direction should)
    threshold = 10
    dropped = [f for f, p in incomplete_features if p > threshold]
    interpolated = interpolated.drop(columns=dropped)

    # init imputer
    imputer = IterativeImputer(max_iter=50, imputation_order="ascending", verbose=1, random_state=SEED)
    imputed = imputer.fit_transform(data.values)
    imputed = pd.DataFrame(index=data.index, columns=data.columns, data=imputed)

    # shoe some plots for visual check
    for col in imputed.columns:
        f = plt.figure(figsize=(20, 6))
        plt.plot(imputed.index, imputed[col])
        plt.title(col)
        plt.show()
        plt.close(f)
    # assert no data is missing
    print(imputed.isna().sum())

    # save to file
    imputed.to_csv(args.out, index=True)