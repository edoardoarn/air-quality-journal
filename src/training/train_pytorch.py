# %%
import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
from sklearn.metrics import r2_score
from matplotlib import pyplot as plt

import torch
from torch.utils.data import DataLoader
from torch.optim import Adam
from torch.nn import MSELoss
from beacon.base import StatefulTrainer, StatelessTrainer, TrainerState
from beacon.models.rnn import LSTMRegressor, StateInit
from beacon.data import SlidingWindowDataset
from beacon import metrics as bm
from beacon.callbacks import PeriodicCheckpoint, MetricsLogger

from src.metrics import smape_score, mae_score, rmse_score
from src.commons import decay_weights

# %%
DATA_PATH = "/mnt/data1/romualdo_data/datasets/air-quality/processed/global"
DATA_NAME = "data_cleaned_cycle_h_rbf.csv"
PROJECT_PATH = "/home/romualdo/projects/_LINKS/air-quality-revenge"
PLOT_PATH = os.path.join(PROJECT_PATH, "plots/notebooks/train_torch", DATA_NAME.replace(".csv", ""))
MODEL_PATH = os.path.join(PROJECT_PATH, "models", DATA_NAME.replace(".csv", ""))
RESULT_PATH = os.path.join(PROJECT_PATH, "results", DATA_NAME.replace(".csv", ""))
TRAIN_RATIO = 0.8
TARGET_VARS = ["pm10","pm25","no2","co","c6h6","nox","bc","so2","nh3","o3"]
PATHS = [MODEL_PATH, PLOT_PATH, RESULT_PATH]

SEED = 42
WINDOW = 24
BATCH_SIZE = 64
N_HIDDEN = 100
N_LAYERS = 3
LR = 1e-2
EPOCHS = 50
STATEFUL = False
DEVICE = "cuda:1"

# %%
for p in PATHS: 
    if not os.path.exists(p):
        os.makedirs(p)

# %%
data = pd.read_csv(os.path.join(DATA_PATH, DATA_NAME), index_col=0, parse_dates=True, date_parser=lambda x: pd.to_datetime(x))
data.dropna(inplace=True)
assert data.isna().sum().sum() == 0
data

# %%
split = int(np.floor(len(data) * TRAIN_RATIO))
X_features = [f for f in data.columns if f not in TARGET_VARS]
Y_features = [f for f in data.columns if f in TARGET_VARS]

train_data, test_data = data.iloc[:split], data.iloc[split:]
X_train_df, Y_train_df = train_data[X_features], train_data[Y_features]
X_test_df, Y_test_df = test_data[X_features], test_data[Y_features]
print(X_train_df.shape, X_test_df.shape, Y_train_df.shape, Y_test_df.shape)

# %%
metrics = [mae_score, rmse_score, r2_score, smape_score]
scaler = StandardScaler().fit(X_train_df)
transformer = FunctionTransformer(func=np.log1p, inverse_func=np.expm1, validate=True)
X_train = scaler.transform(X_train_df)
X_test = scaler.transform(X_test_df)
Y_train, Y_test = Y_train_df.values, Y_test_df.values

# %%
results = list()
for t, target in enumerate(TARGET_VARS):
    train_loader = DataLoader(SlidingWindowDataset(X_train, transformer.transform(Y_train[:,t].reshape(-1, 1)), window=WINDOW), 
                    batch_size=BATCH_SIZE, 
                    num_workers=4, 
                    shuffle=not STATEFUL, 
                    drop_last=STATEFUL)
    test_loader = DataLoader(SlidingWindowDataset(X_test, transformer.transform(Y_test[:,t].reshape(-1, 1)), window=WINDOW), 
                    batch_size=BATCH_SIZE, 
                    num_workers=4, 
                    drop_last=STATEFUL)

    model = LSTMRegressor(input_dim=X_train.shape[1], hidden=N_HIDDEN, layers=N_LAYERS, classes=1, dropout=0.5, state_init=StateInit.RAND)
    trainer_cls = StatefulTrainer if STATEFUL else StatelessTrainer
    trainer = trainer_cls(
        model=model, 
        criterion=MSELoss(), 
        optimizer=Adam(model.parameters(), lr=LR),
        metrics={"mse": bm.mse},
        epochs=EPOCHS)
    final_path = os.path.join(MODEL_PATH, target)
    trainer = trainer.to(DEVICE).register(PeriodicCheckpoint(path=final_path))

# %%
    model_name = str(type(model).__name__).lower().replace("regressor", "")
    model_name = f"{model_name}_{'stateful' if STATEFUL else 'stateless'}_{N_HIDDEN}x{N_LAYERS}_{WINDOW}"
    trainer = trainer.fit(train_loader)
    trainer.save(path=os.path.join(final_path, f"{model_name}_final.pt"))

# %%
    y_pred, (y_true, _) = trainer.evaluate(test_loader)
    y_pred = transformer.inverse_transform(y_pred.cpu().detach().numpy())
    y_true = transformer.inverse_transform(y_true.cpu().detach().numpy())

# %%
    limit = len(X_test_df)
    if STATEFUL:
        limit -= len(Y_test) % BATCH_SIZE
    xx = X_test_df.index.values[WINDOW:limit]
    print(y_pred.shape, y_true.shape, xx.shape)

    scores = [f(y_true, y_pred) for f in metrics]
    for m, score in zip(metrics, scores):
        metric_name = m.__name__
        name = f"{model_name} - {metric_name}"
        print(f"{name:<40s}: {score:04.4f}")
        results.append([target, model_name, metric_name, score])
    print("-"*20)

    fig = plt.figure(figsize=(50, 6))
    plt.plot(xx, y_true, color="silver", linestyle="--")
    plt.plot(xx, y_pred, color="turquoise")
    plt.tight_layout()
    plt.title(model_name)
    plt.savefig(os.path.join(PLOT_PATH, f"{target}_{model_name}.png"))
    plt.close(fig)

results_df = pd.DataFrame(results, columns=["feature", "model", "metric", "score"])    
results_df.to_csv(os.path.join(RESULT_PATH, f"run01_{model_name}.csv"), index=False)


