import pandas as pd
import numpy as np
import argparse
import glob
import os


DEFAULT_DATA_PATH = "D:/Datasets/air-quality/raw/pollution"
DEFAULT_OUTPUT_PATH = "D:/Datasets/air-quality/processed/pollutants"
SENSOR_COORDS = "sensors_coord.csv"
YEARS = ["2013", "2014", "2015"]
OLD_COL_NAMES = ['Id_Stazione', 'Nome_Stazione', 'Id_Sensore', 'Nome_Sensore', 'Unita_Misura', 'Periodo_Dal', 'Periodo_Al', 'Operatore']
NEW_COL_NAMES = ["station_id", "station_name", "sensor_id", "sensor_name", "unit", "from", "to", "operator"]
SENSOR_DICT = {
    'PM10 (SM2005)': 'pm10', 
    'Particelle sospese PM2.5': 'pm25',
    'Biossido di Azoto': 'no2',
    'Monossido di Carbonio': 'co',
    'Benzene': 'c6h6',
    'Ossidi di Azoto': 'nox', 
    'BlackCarbon': 'bc',
    'Biossido di Zolfo': 'so2',
    'Ammoniaca': 'nh3',
    'Ozono': 'o3',
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Converts the many CSVs into a single file")
    parser.add_argument("--path", "-p", type=str, default=DEFAULT_DATA_PATH)
    parser.add_argument("--out", "-o", type=str, default=DEFAULT_OUTPUT_PATH)
    parser.add_argument("--coords", "-c", type=str, default=SENSOR_COORDS)

    args = parser.parse_args()
    columns = dict(list(zip(OLD_COL_NAMES, NEW_COL_NAMES)))

    # search and save all filenames
    files = list()
    for sub in YEARS:
        paths = os.path.join(args.path, sub, "Legenda", "*.csv")
        files += glob.glob(paths)
    print(f"Found {len(files)} legend files")

    # prepare empty dataframe
    sensors = pd.DataFrame(columns=list(columns.keys()))

    # load each description file and add it
    # delete station name from sensor name (only happens to pollutants)
    for f in files:
        current = pd.read_csv(f)
        edit = "Nome_Sensore"
        for i, row in current.iterrows():
            sensor_name = row[edit].replace(row["Nome_Stazione"], "")
            current.at[i, edit] = sensor_name.strip()
        sensors = sensors.append(current)
    sensors.rename(columns=columns, inplace=True)
    
    # print some info
    unique_sensors = sensors.sensor_id.unique()
    unique_count = len(unique_sensors)
    for col in columns.values():
        print(col)
        print(sensors[col].unique())
        print(f"count: {len(sensors[col].unique())}")
        print("-"*20)
    
    # drop periods (useless, we know them) and drop duplicate rows
    sensors = sensors.drop(["from", "to"], axis=1).drop_duplicates().reset_index(drop=True)
    # check we still got everything (and nothing more)
    assert(unique_count == len(sensors))

    # load coordinates
    coords = pd.read_csv(os.path.join(args.path, SENSOR_COORDS))
    sensors = pd.merge(sensors, coords, on="sensor_id", how="left")
    assert(unique_count == len(sensors) == len(sensors["sensor_id"].unique()))

    # convert ids to numeric
    sensors.sensor_id = pd.to_numeric(sensors.sensor_id)
    sensors.station_id = pd.to_numeric(sensors.station_id)
    
    # load sensor measurements
    files = list()
    for sub in YEARS:
        files += glob.glob(os.path.join(args.path, sub, "*.csv"))
    
    # create empty dataframe
    measurements = pd.DataFrame()
    num_rows = 0

    sensor_ids = []
    for f in files:
        current = pd.read_csv(f, encoding="utf_8_sig") #csvs marked with BOM
        # rename columns and convert values
        current.columns = ["sensor_id", "timestamp", "value"]
        current.timestamp = pd.to_datetime(current.timestamp)
        current.sensor_id = pd.to_numeric(current.sensor_id)
        current.value = pd.to_numeric(current.value)
        uniques = current.sensor_id.unique()
        assert(len(uniques) == 1)
        sensor_ids.append(uniques[0])
        # append data
        measurements = measurements.append(current, ignore_index=True)
        num_rows += len(current)
    
    # compare counts
    print("unique sensors")
    print(unique_sensors)
    print(f"found {len(sensor_ids)}")
    print(f"unique: {len(np.unique(np.array(sensor_ids)))}")
    print(sensor_ids)
    missing = [x for x in sensor_ids if x not in sensors.sensor_id.values]
    print(f"missing {len(missing)}")
    print(missing)
    # check dimensions
    assert(num_rows == len(measurements))

    # merge sensor names and ids to make unique names
    merged_columns = []
    id_to_name = {}
    for s_id in unique_sensors:
        info = sensors[sensors.sensor_id == s_id]
        assert(len(info) == 1)
        s_name = SENSOR_DICT[info.iloc[0].sensor_name]
        merged_name = f"{s_name}_{s_id}"
        id_to_name[s_id] = merged_name
        merged_columns.append(merged_name)
    print(merged_columns)

    # make new dataframe with timestamps at fixed resolution
    index = pd.date_range(start=min(measurements.timestamp), end=max(measurements.timestamp), freq="H")
    print(f"dataframe from {min(index)} to {max(index)}")
    # check we have 3 years * 365 days ad 24 hours a day, + midnight of 01-01-2016
    # minus 2 hours since it only starts from 2 am for pollutants
    assert(len(index) == 3 * 365 * 24 + 1 - 2)
    merged = pd.DataFrame(index=pd.DatetimeIndex(index), columns=merged_columns)

    # insert measurements indie the empty df
    for sensor in unique_sensors:
        print(f"adding sensor: {sensor} ({id_to_name[sensor]})")
        subset = measurements[measurements.sensor_id == sensor]
        subset = subset.drop_duplicates(subset="timestamp")
        period = pd.DatetimeIndex(subset.timestamp)
        # sanity checks to see if we have external dates, or the dates are longer
        print([a for a in subset.timestamp.values if a not in index.values])
        assert len(subset) <= len(index)

        column = id_to_name[sensor]
        print(f"found column :{column} - df length: {len(merged.loc[period, column])}")
        merged.loc[period, column] = subset["value"].values
    
    print(merged.shape)
    merged = merged.replace(-999, np.nan)
    print(merged.isna().sum())
    print(merged.head())

    # save to file the merged results without index (we'll load using timestamp as index)
    merged.to_csv(os.path.join(args.out, "data_merged.csv"), index=True)



